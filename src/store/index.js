import { createStore } from 'vuex'
import ParsingData from '@/assets/data/variants.json'

export default createStore({
  strict: process.env.NODE_ENV !== 'production',
  state: {
    ParsingData: ParsingData.variants,
    resultData: [],
    sortAsc: true,
    filterSignificance: [
      {name: 'PATHOGENIC', trg: true},
      {name: 'LIKELY_PATHOGENIC', trg: true},
      {name: 'BENIGN', trg: true},
      {name: 'UNDEFINED', trg: true},
      {name: 'UNCERTAIN', trg: true},
      {name: 'LIKELY_BENIGN', trg: true}
    ],
    filterSignificanceResult: [],
    filterInputItem: [
      {name: 'NameVar', value: 'Имя' },
      // {name: 'FormG', value: 'FormG' },
      // {name: 'FormC', value: 'FormC' },
      // {name: 'FormP', value: 'FormP' }
    ],
    filterInputName: ''
  },
  getters: {
  },
  mutations: {
    FILTER_SIGNIFICANSE(s,type){
      let result =  s.filterSignificanceResult;

      for(var i=0; i < s.filterSignificance.length; i++){
        let item = s.filterSignificance[i];
        if (item.name == type && item.trg){
          item.trg = false
        }else if(item.name == type && !item.trg){
          item.trg = true
        }

        if (item.trg){
          result.push(item.name)
        }
      }

      console.log(result)
      s.resultData = s.ParsingData.filter(i => result.includes(i.significance))
      result.length = 0
    },

    FILTER_INPUT(s,input){
      console.log(input.value)
      if (input.name === 'NameVar'){
        s.resultData = s.ParsingData.filter(i => i.alleleName.includes(input.value))
      }
      // if (input.name === 'FormG'){}
    },
    SORT_DATA(s,column){//нужна либо пагинация, либо после фильтрации
      let ascDesc = s.sortAsc ? 1 : -1;
      console.log(column);
      s.resultData.sort((a, b) => ascDesc * a.genotype.localeCompare(b.genotype));
      s.sortAsc = false
    }
  },
  actions: {
    FILTER_SIGNIFICANSE({commit},type){
      commit('FILTER_SIGNIFICANSE', type)
    },
    FILTER_INPUT({commit},input){
      commit('FILTER_INPUT', {name: input.name, value: input.value})
    },
    SORT_DATA({commit}, column){
      commit('SORT_DATA', column)
    }
  },
  modules: {
  }
})