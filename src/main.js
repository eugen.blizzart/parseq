import { createApp } from 'vue'
import App from './App.vue'
import store from './store'
import './assets/less/main.less'
//components
import TheHeader from './TheHeader'

const app = createApp(App);
app.use(store);

app.component('the-header', TheHeader);

app.mount('#app');
